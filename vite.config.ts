import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// noinspection JSUnusedGlobalSymbols
export default defineConfig({
    base: '/vut-subject-market-frontend/',
    plugins: [vue()],
    server: {
        // proxy: {
        //     '^/api/v1': {
        //         target: 'http://localhost:4321',
        //         changeOrigin: true
        //     }
        // }
        proxy: {
            '^/api/v1': {
                target: 'http://2042175058.kn.vutbr.cz:4321',
                changeOrigin: true
            }
        }
    }
})