import { createApp } from 'vue'
import './style.scss'
import App from './App.vue'
import router from './router/Router.vue'
import axios from 'axios';
import VueAxios from 'vue-axios';

const app = createApp(App)
app.use(router)
app.use(VueAxios, axios)
app.mount('#app')